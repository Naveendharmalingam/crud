from flask import Flask,jsonify,request
from pymysql.cursors import DictCursor
import pymysql
from logging import Logger


app=Flask(__name__)
def mysqlconnect():
    conn=pymysql.connect(
        host='localhost',
        user='root',
        password='',
        db='sample_db'
    )
    return conn

def excute_query(query,qtype):
    conn=mysqlconnect()
    conn.cursorclass=DictCursor
    cur=conn.cursor()
    cur.execute(query)
    if qtype.upper()=="SELECT":
        return jsonify(cur.fetchall())
    else:
        conn.commit()
        return jsonify("successfuly"+ qtype)


@app.route('/query',methods=['GET','POST'])
@app.route('/query/<int:empid>',methods=['GET','PUT','DELETE'])
@app.route('/',methods=['GET','POST'])
def home(empid=None):
    data=request.get_json()
    conn=mysqlconnect()
    conn.cursorclass=DictCursor
    cur=conn.cursor()
    if request.method=="GET":
        if empid!=None:
            return excute_query("SELECT * FROM employee_tbl WHERE empid={}".format(empid),"select") 
        else:
            return excute_query("SELECT * FROM employee_tbl","select")
    
    elif request.method=='POST':
        query='INSERT INTO employee_tbl ({}) VALUES {}'.format(",".join([i for i in list(data)]),tuple(data.values())) 
        cur.execute(query)
        conn.commit()
        return jsonify("insert successful")

    elif request.method=="PUT":
        val=''
        query="UPDATE employee_tbl SET "
        for i,j in data.items():
            val+="{}='{}',".format(i,j)
        query=query+val[:-1]+" WHERE empid={}".format(empid)
        cur.execute(query)
        conn.commit()
        return "update successful"
    else:
        return excute_query("DELETE FROM employee_tbl WHERE empid={}".format(empid),'delete')

if __name__=='__main__':
    app.run(debug=True)
